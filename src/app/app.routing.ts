import { RouterModule, Routes } from '@angular/router';

//importmaos componentes
import {MedicamentosComponent} from './components/medicamentos/medicamentos.component';
import {HomeComponent} from './components/home/home.component';
import {LaboratorioComponent} from './components/laboratorio/laboratorio.component';
import { FormComponent } from './components/medicamentos/form/form.component';

const APP_ROUTES: Routes = [
  { path: 'proveedores', component: HomeComponent },
  { path: 'medicamentos', component: MedicamentosComponent,
  children:[
    { path: '', redirectTo:'form',pathMatch:'full' },
    { path: 'form', component: FormComponent }
  ]
 },
  { path: 'laboratorio', component: LaboratorioComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'proveedores' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
